<!DOCTYPE html>
<!-- Template Name: Clip-One - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.3 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->


<!-- start: HEADER -->

<!-- /.modal -->
<!-- end: SPANEL CONFIGURATION MODAL FORM -->
<div class="panel-body">
    <!-- start: PAGE HEADER -->

    <!-- end: PAGE HEADER -->
    <!-- start: PAGE CONTENT -->
    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            
            
            <div class="row">
						<div class="col-sm-7" id="pkg_div">
							<div class="panel panel-default">
								<div class="panel-heading">
									<i class="clip-stats"></i>
									New Package
									
								</div>
								<div class="panel-body">
                    <form  class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="form-field-1">
                                From
                            </label>
                            <div class="col-sm-4">
                                <input type="text" required="" autofocus list="booking_fr" onkeyup="set_package_code()"  autocomplete="on"   placeholder="Insert From" class="form-control" id="booking_from" >
                                <datalist   id="booking_fr">

                                    <?php
                                    include './classes/DB.php';
                                    include './classes/load_place.php';

                                    
                                    ?>



                                </datalist>

                            </div>
                            
                            <label class="col-sm-2 control-label" for="form-field-1">
                                To
                            </label>
                            <div class="col-sm-4">
                                <input type="text" required="" autofocus list="book_to" onkeyup="set_package_code()"  autocomplete="on"   placeholder="Insert To" class="form-control" id="booking_to" >
                                <datalist   id="book_to">

                                    <?php
                                    
                                    include './classes/DB.php';
                                    include './classes/load_place.php';

                                    
                                    
                                    ?>



                                </datalist>

                            </div>
                            
                        </div>

                        <div class="form-group ">
                            <label class="col-sm-2 control-label"  for="form-field-4" >
                                Days 
                            </label>

                            <div class="col-sm-4">

                                <input type="text" id="days" onkeyup="set_package_code()" class="form-control">
                            </div>
                            <label class="col-sm-2 control-label" for="form-field-4" >
                                Kms 
                            </label>

                            <div class="col-sm-4">

                                <input type="text" id="kms" onkeyup="set_package_code()" class="form-control">
                            </div>

                        </div>
                        <div class="form-group ">
                            <label class="col-sm-2 control-label" for="form-field-4" >
                                Package Amount
                            </label>
                            <div class="col-sm-10">
                                <input type="text" id="pkg" onkeyup="set_package_code()" class="form-control">
                            </div>
                        </div>
                        
                        <div class="form-group ">
                            <label class="col-sm-2 control-label" for="form-field-5" >
                                Special
                            </label>
                            <div class="col-sm-10">
                                <input type="text" id="rem" onkeyup="set_package_code()" class="form-control">
                            </div>
                        
                            
                        
                        </div>
                        <div class="form-group ">
                            <label class="col-sm-2 control-label" for="form-field-6">
                                Package Code
                            </label>
                            <div class="col-sm-10">
                                <input disabled="" style="text-align: center;background-color: #ff6666;font-size:15px;color: #ffffff;"  type="text" id="p_code"   class="form-control label-info">

                            </div>
                        </div>
                        

                        <hr>

                        <div class="form-group">
                            <div class="col-md-8"></div>

                            <div class="col-md-3">
                                <button onclick="add_new_package()"  class="btn btn-yellow btn-block" type="button">
                                    Add Package 
                                </button>
                            </div>

                        </div>
                    </form>
                </div>
							</div>
						</div>
						<div class="col-sm-5">
							<div class="row">
								<div class="col-sm-12">
									<div class="panel panel-default">
										<div class="panel-heading">
											<i class="clip-pie"></i>
											Package List
											
										</div>
										<div class="panel-body">
											
												<ul class="todo">
                                                                                                    <?php
                                                                                                    
                                                                                                        include './classes/Load_Package_list.php';
                                                                                                    
                                                                                                    ?>
										
                                                                                                </ul>
											
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>






    <!-- end: PAGE CONTENT-->
</div>

<!-- end: PAGE -->

<!-- end: MAIN CONTAINER -->
<!-- start: FOOTER -->

<!-- end: FOOTER -->
<!-- start: MAIN JAVASCRIPTS -->
<!--[if lt IE 9]>
<script src="assets/plugins/respond.min.js"></script>
<script src="assets/plugins/excanvas.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->

<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
