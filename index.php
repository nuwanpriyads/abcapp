<!DOCTYPE html>
<!-- Template Name: Clip-One - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.3 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">

    <!--<![endif]-->
    <!-- start: HEAD -->
    <head>

        <title>CabZ - Cabs Control and Management System</title>
        <!-- start: META -->
        <meta charset="utf-8" />
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- end: META -->
        <!-- start: MAIN CSS -->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/fonts/style.css">
        <link rel="stylesheet" href="assets/css/main.css">
        <link rel="stylesheet" href="assets/css/main-responsive.css">
        <link rel="stylesheet" href="assets/plugins/iCheck/skins/all.css">
        <link rel="stylesheet" href="assets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css">
        <link rel="stylesheet" href="assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
        <link rel="stylesheet" href="assets/css/theme_green.css" type="text/css" id="skin_color">
        <link rel="stylesheet" href="assets/css/print.css" type="text/css" media="print"/>
        <!--[if IE 7]>
        <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
        <![endif]-->
        <!-- end: MAIN CSS -->
        <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
        <link href="assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
        <link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="assets/plugins/fullcalendar/fullcalendar/fullcalendar.css">
        <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
        <link rel="shortcut icon" href="favicon.ico" />

        <script type="text/javascript">
                    
   
   
    
    
            function set_page(urlx, type) {
                var url = urlx;
                $.ajaxSetup({
                    cache: false
                });
                if(urlx=="New_Booking.php"){
                    document.getElementById("new_bookings").className="active";
                    document.getElementById("bookings").className="";
                    document.getElementById("driver").className="";
                    document.getElementById("packages").className="";
                    document.getElementById("vehicle").className="";
                }else if(urlx=="Driver.php"){
                    document.getElementById("driver").className="active";
                    document.getElementById("bookings").className="";
                    document.getElementById("new_bookings").className="";
                    document.getElementById("packages").className="";
                    document.getElementById("vehicle").className="";
                }else if(urlx=="Packages.php"){
                    document.getElementById("packages").className="active";
                    document.getElementById("bookings").className="";
                    document.getElementById("new_bookings").className="";
                    document.getElementById("driver").className="";
                    document.getElementById("vehicle").className="";
                }else if(urlx=="Vehicle.php"){
                    document.getElementById("vehicle").className="active";
                    document.getElementById("packages").className="";
                    document.getElementById("bookings").className="";
                    document.getElementById("new_bookings").className="";
                    document.getElementById("driver").className="";
                    
                }
                

                if (type != 'key') {


                } else {
                    //var keyID = (event.charCode) ? event.charCode : ((event.which) ? event.which : event.keyCode);

                    var keyID = event.which;

                    if (keyID == 112) {

                        url = 'Add_New_Item.php';
                    } else {

                        return false;
                    }
                }


                $.post(url, null, function (res) {

                    document.getElementById("content_set").innerHTML = res;
                    if(urlx == 'New_Booking.php'){
                        Genarated();  
                        
                    }

                    FormValidator.init();
                    FormElements.init();
                    UIModals.init();
                    
                    
                    //Index.init();


                }, 'text');

            }

            

            
            
            function user_logout(){
                       
                $.ajaxSetup ({
                    cache: false
                });
                
                $.post("classes/user_logout.php",null,function(res){
                    
                    if(res=="success"){
                        window.location.assign("index.php");
            
                    }
                    
       
			
                } , 'text');

            }



        </script>

        <style>
            .control-label{

                padding-bottom:15px;

            }



        </style>

    </head>
    <!-- end: HEAD -->
    <!-- start: BODY -->
    <body>
        <?php
        include 'form-calendar.php';
        ?>

        <?php
        include 'classes/DB.php';
        session_start();

        if (!isset($_SESSION['login_user'])) {
            header("location: login.php");
        } else {

            $username = $_SESSION['login_user'];

            $query2 = "select * from user_login where user_name='$username'";

            $ses_sql1 = mysqli_query($con, $query2);
            $row1 = mysqli_fetch_assoc($ses_sql1);
            $result1 = $row1['GUP_idGUP'];

            $query3 = "select * from gup where idGUP='$result1'";

            $ses_sql2 = mysqli_query($con, $query3);
            $row2 = mysqli_fetch_assoc($ses_sql2);
            $result2 = $row2['fname'];
            $result3 = $row2['lname'];
        }
        ?>
        <!-- start: HEADER -->
        <div class="navbar navbar-inverse navbar-fixed-top">
            <!-- start: TOP NAVIGATION CONTAINER -->
            <div class="container">
                <div class="navbar-header">
                    <!-- start: RESPONSIVE MENU TOGGLER -->
                    <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                        <span class="clip-list-2"></span>
                    </button>
                    <!-- end: RESPONSIVE MENU TOGGLER -->
                    <!-- start: LOGO -->
                    <a class="navbar-brand" href="index.php">
                        CabZ
                    </a>
                    <!-- end: LOGO -->
                </div>
                <div class="navbar-tools">
                    <!-- start: TOP NAVIGATION MENU -->
                    <ul class="nav navbar-right">
                        <!-- start: TO-DO DROPDOWN -->
                        <li class="dropdown">
                            <?php
                            $today = Date('Y-m-d');

                            $pending_bookings = mysqli_query($con, "select * from booking where className='label-green' and start='$today'");

                            $rows_c = mysqli_num_rows($pending_bookings);
                            ?>

                            <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
                                <i class="clip-list-5"></i>
                                <span class="badge"><?php echo $rows_c ;?></span>
                            </a>
                            <ul class="dropdown-menu todo">
                                <li>
                                    <span class="dropdown-menu-title"> You have pending tasks</span>
                                </li>
                                <?php
                                if ($rows_c >= 1) {

                                    while ($package_row_q = mysqli_fetch_array($pending_bookings)) {
                                        $booking_to = $package_row_q['booking_to'];
                                        $booking_number = $package_row_q['booking_no'];
                                        $start_date = $package_row_q['start'];
                                        ?>

                                        <li> 
                                            <a href="#booking-management" data-toggle="modal" onclick="set_page_booking_details('<?php echo $package_row_q['title'] ?>')">
                                                <span class="desc" style="opacity: 1; text-decoration: none;"><?php echo $booking_to . "-" . $booking_number ?>  </span>
                                                <span class="label label-danger" style="opacity: 1;"> today</span>
                                            </a>

                                        </li>



        <?php
    }
} else {
    ?>
                                    <li> 
                                        <a class="todo-actions" href="javascript:void(0)">
                                            <span class="desc" style="opacity: 1; text-decoration: none;">No Results Found</span>
                                        </a>



                                    </li>
<?php }
?>
                                <li class="view-all">
                                    <a href="javascript:void(0)">
                                        See all tasks <i class="fa fa-arrow-circle-o-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- end: TO-DO DROPDOWN-->
                        <!-- start: NOTIFICATION DROPDOWN -->
                        <li class="dropdown">
                            <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
                                <i class="clip-notification-2"></i>
                                <span class="badge"> 0</span>
                            </a>
                            <ul class="dropdown-menu notifications">
                                <li>
                                    <span class="dropdown-menu-title"> You have No notifications</span>
                                </li>

                                <li class="view-all">
                                    <a href="javascript:void(0)">
                                        See all notifications <i class="fa fa-arrow-circle-o-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- end: NOTIFICATION DROPDOWN -->
                        <!-- start: MESSAGE DROPDOWN -->
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-close-others="true" data-hover="dropdown" data-toggle="dropdown" href="#">
                                <i class="clip-bubble-3"></i>
                                <span class="badge"> 0</span>
                            </a>
                            <ul class="dropdown-menu posts">
                                <li>
                                    <span class="dropdown-menu-title"> You have No messages</span>
                                </li>

                                <li class="view-all">
                                    <a href="javascript:void(0)">
                                        See all messages <i class="fa fa-arrow-circle-o-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- end: MESSAGE DROPDOWN -->
                        <!-- start: USER DROPDOWN -->
                        <li class="dropdown current-user">
                            <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
                                <img src="assets/images/avatar-1-small.jpg" class="circle-img" alt="">
                                <span class="username"><?php echo $result2 . " " . $result3 ?></span>
                                <i class="clip-chevron-down"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="#">
                                        <i class="clip-user-2"></i>
                                        &nbsp;My Profile
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="clip-calendar"></i>
                                        &nbsp;My Calendar
                                    </a>
                                <li>
                                    <a href="#">
                                        <i class="clip-bubble-4"></i>
                                        &nbsp;My Messages (0)
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="utility_lock_screen.html"><i class="clip-locked"></i>
                                        &nbsp;Lock Screen </a>
                                </li>
                                <li>
                                    <a onclick="user_logout()" href="#">
                                        <i class="clip-exit"></i>
                                        &nbsp;Log Out
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- end: USER DROPDOWN -->
                    </ul>
                    <!-- end: TOP NAVIGATION MENU -->
                </div>
            </div>
            <!-- end: TOP NAVIGATION CONTAINER -->
        </div>
        <!-- end: HEADER -->
        <!-- start: MAIN CONTAINER -->
        <div class="main-container">
            <div class="navbar-content">
                <!-- start: SIDEBAR -->
                <div class="main-navigation navbar-collapse collapse">
                    <!-- start: MAIN MENU TOGGLER BUTTON -->
                    <div class="navigation-toggler">
                        <i class="clip-chevron-left"></i>
                        <i class="clip-chevron-right"></i>
                    </div>
                    <!-- end: MAIN MENU TOGGLER BUTTON -->
                    <!-- start: MAIN NAVIGATION MENU -->
                    <ul class="main-navigation-menu">
                        <li id="bookings" class="active">
                            <a href="index.php"><i class="clip-home-3"></i>
                                <span class="title"> Bookings </span><span class="selected"></span>
                            </a>
                        </li>
                        <li id="new_bookings" >
                            <a href="#" onclick="set_page('New_Booking.php','')"><i class="clip-calendar"></i>
                                <span class="title"> New Bookings </span><span class="selected"></span>
                            </a>
                        </li>
                        <li id="packages" >
                            <a href="#" onclick="set_page('Packages.php','')"><i class="clip-cube"></i>
                                <span class="title"> New Package </span><span class="selected"></span>
                            </a>
                        </li>
                        <li id="driver" >
                            <a href="#" onclick="set_page('Driver.php','')"><i class="clip-user-plus"></i>
                                <span class="title"> New Driver </span><span class="selected"></span>
                            </a>
                        </li>
                        <li id="vehicle" >
                            <a href="#" onclick="set_page('Vehicle.php','')"><i class="clip-cart"></i>
                                <span class="title"> New Vehicle </span><span class="selected"></span>
                            </a>
                        </li>



                    </ul>
                    <!-- end: MAIN NAVIGATION MENU -->
                </div>
                <!-- end: SIDEBAR -->
            </div>
            <!-- start: PAGE -->
            <div class="main-content">
                <!-- start: PANEL CONFIGURATION MODAL FORM -->
                <div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                    &times;
                                </button>
                                <h4 class="modal-title">Panel Configuration</h4>
                            </div>
                            <div class="modal-body">
                                Here will be a configuration form
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                    Close
                                </button>
                                <button type="button" class="btn btn-primary">
                                    Save changes
                                </button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <!-- end: SPANEL CONFIGURATION MODAL FORM -->
                <div class="container">
                    <!-- start: PAGE HEADER -->
                    <div class="row">

                    </div>
                    <!-- end: PAGE HEADER -->
                    <!-- start: PAGE CONTENT -->
                    <div class="row" id="content_set" >
                        <div class="col-sm-12" >
                            <!-- start: FULL CALENDAR PANEL -->
                            <div class="panel panel-default" >

                                <div class="panel-body" >
                                    <div class="col-sm-11">
                                        <div id='calendar'></div>
                                    </div>
                                    <div class="col-sm-1">
                                        <h4>Booking Status</h4>
                                        <div id="event-categories">
                                            <div class="event-category label-yellow" data-class="label-green">
                                                <i class="fa fa-move"></i>
                                                Running
                                            </div>
                                            <div class="event-category label-green" data-class="label-default">
                                                <i class="fa fa-move"></i>
                                                Ahead 
                                            </div>
                                            <div class="event-category label-orange" data-class="label-orange">
                                                <i class="fa fa-move"></i>
                                                Past 
                                            </div>
                                            <div class="event-category label-default" data-class="label-default">
                                                <i class="fa fa-move"></i>
                                                Cancel 
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end: FULL CALENDAR PANEL -->
                        </div>
                    </div>
                    <!-- end: PAGE CONTENT-->
                </div>
            </div>
            <!-- end: PAGE -->
        </div>
        <!-- end: MAIN CONTAINER -->
        <!-- start: FOOTER -->
        <div class="footer clearfix">
            <div class="footer-inner">
<?php echo Date('Y'); ?> &copy; CABZ by Sithuvili Concepts.
            </div>
            <div class="footer-items">
                <span class="go-top"><i class="clip-chevron-up"></i></span>
            </div>
        </div>
        <!-- end: FOOTER -->
        <div id="event-management" class="modal fade" tabindex="-1"  data-width="900" style="display: none; font-weight: bold; ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">Booking Management</h4>
            </div>
            <div  class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                    Close
                </button>

            </div>
        </div>
        <div id="booking-management" class="modal fade" tabindex="-1"  data-width="900" style="display: none; font-weight: bold; ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">Booking Management</h4>
            </div>
            <div id="page-load" class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                    Close
                </button>

            </div>
        </div>

        <!-- start: MAIN JAVASCRIPTS -->
        <!--[if lt IE 9]>
        <script src="assets/plugins/respond.min.js"></script>
        <script src="assets/plugins/excanvas.min.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <![endif]-->
        <!--[if gte IE 9]><!-->
        <script src="jquery.js"></script>
        <!--<![endif]-->
        <script src="assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
        <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
        <script src="assets/plugins/blockUI/jquery.blockUI.js"></script>
        <script src="assets/plugins/iCheck/jquery.icheck.min.js"></script>
        <script src="assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
        <script src="assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
        <script src="assets/plugins/less/less-1.5.0.min.js"></script>
        <script src="assets/plugins/jquery-cookie/jquery.cookie.js"></script>
        <script src="assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
        <script src="assets/js/main.js"></script>
        <script src="main.js"></script>
        <!-- end: MAIN JAVASCRIPTS -->
        <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script src="assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
        <script src="assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
        <script src="assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
        <script src="assets/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
        <script src="assets/plugins/fullcalendar/fullcalendar/fullcalendar.js"></script>

        <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script>
            jQuery(document).ready(function() {
                Main.init();
                Calendar.init();
            });
        </script>
    </body>
    <!-- end: BODY -->
</html>